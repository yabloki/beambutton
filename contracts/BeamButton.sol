pragma solidity 0.5.0;


/** @author Kirill Moizik
 @title A simple button game. 
 If no one pressed the button for the last 3 blocks, the last player
 will get all the money in the contract **/
contract BeamButton {
    
    uint public predefinedBid;
    address payable public latestPlayer;
    uint public latestBlock;
    uint public treasure;
    bool private ended;

    event BeamButtonPressed(address player);
    event GameEnded(address winner, uint amount);

    modifier onlyWhileGameOn() { 
        require(!ended, "Game already Ended");
        require(
            latestBlock == 0 || block.number <= latestBlock + 3,
            "Game already Ended"
        );
         _; }
    
    modifier onlyWhileGameOver() { 
        require(
            latestBlock != 0 && block.number > latestBlock + 3,
             "Game still on");
         _; }

    /// Create a new Button contract with specific `bid`.
    constructor(uint bid) public {
        require(bid > 0, "Step have to be bigger than 0");
        predefinedBid = bid;
    }

    /// Press the button.
    /// @dev expects to recieve exact value of `predefinedBid`
    function pressButton() external payable onlyWhileGameOn {
        require(
            msg.value == predefinedBid,
            "The amount have to be equal to predefinedBid"
        );

        latestPlayer = msg.sender;
        latestBlock = block.number;
        treasure += msg.value;
        emit BeamButtonPressed(latestPlayer);
    }

    /// Claim the reward.
    /// @dev despite who pressed the button, the winner will get the reward
    function claimTreasure() external onlyWhileGameOver {
        require(!ended, "Game over. Treasure has been claimed");
        ended = true;
        emit GameEnded(latestPlayer, treasure);
        // Checks Effects Interactions pattern 
        // it is an overkill here due to ended will not allow re-entrance attack anyway
        uint amount = treasure;
        treasure = 0;

        // transfer method chosen
        // according to Secure Ether Transfer pattern     
        latestPlayer.transfer(amount);   
    }
}

