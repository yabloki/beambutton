# **Project Beam button**
## **Description**
   
   Beam button is an ethereum smart contract written in Solidity that is similar to The Button on Reddit,  participants pay a fixed amount of ether press the button, 
   and then if 3 blocks pass without someone pressing the button, whoever pressed the button last can call claim the treasure.
   
   It is important to add that it is an introductory level project implemented less than in 3 days. 
   The goal was to get fast acquaintance with the world of ethereum smart contracts, learn best practices, system foundation, tools and frameworks etc
  
## **Assumptions**
   1.  Only predefined fixed size of deposits are allowed
   2.  The game starts after the first player makes the deposit
   3.  When the game finishes, anyone can call claim reward function, the winner will receive it.
   4.  A contract designed to play the game only once. When the game is over there is no way to reinitiate it on the same deployed contract

## **Roadmap and Current State**
### Project implementation :
     1. implement contract logic with help of truffle suite
     2. add solidity unit  tests 
     3. add JS integration tests
     4. Refactor code according to best practices/ security patterns
     5. run Slither as security inspector/static analysis and fix the warnings.

### Planned but wasn't done due to time constraints:
     7. Add TypeScript, 
     8. Create CI pipeline that includes code-coverage, lint, static analysis, dynamic analysis (echidna).
     8. Deploy contract to ropsten infura
     9. create some basic UI with Drizzle for the game
    10. add Moesif for contract monitoring 

# **Usage**
 Clone project

`   git clone git@bitbucket.org:yabloki/beambutton.git   `  

    
 Install truffle
    
` npm install -g truffle   `

 Install truffle assertions module
 
` npm install truffle-assertions  `
 
 Install ganache
    
` npm install -g ganache-cli   `

Run  ganache:

`   ganache-cli -p 7545   `

Deploy Contract

  `  truffle migrate --network development --fixed_bid 1   `
  
Or run full tests suite 

  `  truffle test --fixed_bid 2   ` Tests will fail with other value than 2, see more in known issues  


# **Known Issues**
 There are knows issue/flaws that weren't fixed due to time limitation of the project.
 
 1.  Tests separation far from the best. Before I started I've read that sol tests used as unit tests and js tests as integration tests, so I tried to stick to it. 
     It was hard to distinguish it properly. I will definitely try a different approach in the next project
 2.  Block generation in the test environment was one of the most time-consuming challenges in this project. It failed me 3 times. First time I spend  a lot of time trying to figure out
     how to generate block in Remix IDE, then within solidity unit tests and then within truffle JS test framework. To be able to finish the project in time I used ugly hacks in all the cases. Not proud to put my name on it at all.
     I found a proper solution here - https://github.com/OpenZeppelin/openzeppelin-test-helpers project, I didn't have time to implement it.
 3.  The "game over" state management happens with state variable ended and dynamic condition evaluation. It seems not elegant and overcomplicated. There is probably a more elegant solution
 4.  Test suite result depends on the hardcoded input value. It was convenient for migrations, can't be used for tests.