pragma solidity 0.5.0;
import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/BeamButton.sol";


contract TestBeamButton {
    // this line worth 4 hours of my life
    uint public initialBalance = 100 wei;

    // this line worth 2 hours of my life
    function () external payable {
    }

    function testInitPredefinedBid() public {
        BeamButton button = new BeamButton(12 wei);
        Assert.equal(button.predefinedBid(), 12 wei, "Bid is different than constructor argument");
    }

    function testInitTreasure() public {
        BeamButton button = new BeamButton(1 wei);
        Assert.equal(button.treasure(), 0 wei, "treasure init value is different than 0");
    }

    function testInitLatestBlock() public {
        BeamButton button = new BeamButton(1 wei);
        Assert.equal(button.latestBlock(), 0 wei, "latest block init value is different than  0");
    }

    function testPressButtonLessAmount() public {
        BeamButton button = new BeamButton(2 wei);
        (bool result, bytes memory returnData) = address(button).call.value(1 wei)(abi.encode(keccak256("pressButton()")));
        Assert.equal(result, false, "Allows values other than fixed");
    }

    function testPressButtonBiggerAmount() public {
        BeamButton button = new BeamButton(1 wei);
        (bool result, bytes memory returnData) = address(button).call.value(2 wei)(abi.encode(keccak256("pressButton()")));
        Assert.equal(result, false, "Allows values other than fixed");
    }
    
    function testPressButtonCorrectAmount() public {
        BeamButton button = new BeamButton(1 wei);
        (bool result, bytes memory returnData)=address(button).call.value(1 wei)(abi.encode(keccak256("pressButton()")));
        Assert.equal(result, true, "Does not accept correct amou");
    }

    function testPressButtonLatestPlayerSet() public payable {
        BeamButton button = new BeamButton(1 wei);
        button.pressButton.value(1 wei)();
        Assert.equal(button.latestPlayer(), address(this), "Pressing the button did not set latest player");
    }

    function testPressButtonLatestBlockSet() public payable {
        BeamButton button = new BeamButton(1 wei);
        button.pressButton.value(1 wei)();
        Assert.equal(button.latestBlock(), block.number, "Pressing the button did not set latest player");
    }
    
    event Log(uint balance);
    function testClaimTreasureTransferStart() public {
        BeamButton button = BeamButton(DeployedAddresses.BeamButton());
        uint balance = address(this).balance;
        button.pressButton.value(2 wei)(); 
        Assert.equal(address(this).balance, balance - 2, "Pressing the button did reduce proper amount of wei");
    }

    // Am I proud ? No.
    function testClaimTreasureSimulateBlock1SHAME() public {
    }
    
    function testClaimTreasureSimulateBlock2SHAME() public {
    }
    
    function testClaimTreasureTransferFailure() public {
        BeamButton button = BeamButton(DeployedAddresses.BeamButton());
        (bool result, bytes memory returnData) = address(button).call(abi.encode(keccak256("claimTreasure()")));
        Assert.equal(result, false, "Allows claim treasure before 3 blocks delay over");
    }
   
    function testClaimTreasureTransferSuccess() public {
        BeamButton button = BeamButton(DeployedAddresses.BeamButton());
        uint balance = address(this).balance;
        uint treasure = button.treasure();
        button.claimTreasure();  
        Assert.equal(address(this).balance, balance  + treasure, "Claiming treasure did not reward winner");
    }
}