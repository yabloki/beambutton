const BeamButton = artifacts.require("BeamButton");
const truffleAssert = require('truffle-assertions');

contract("BeamButton integration tests", async accounts => {
    
    block = async () => await web3.eth.getBlock("latest");
    
    mineBlocks = async (blocks) => {
        for (i=0; i<blocks; i++){
            await web3.eth.sendTransaction({from: accounts[1], value: 1});
        }
    };
    
    simulateGameOver = async contractInstance => {
        await contractInstance.pressButton({from: accounts[0], value: 2});
        await mineBlocks(3)
        await contractInstance.claimTreasure({from: accounts[0]}) ;
    };
    
    it("should change a latestPlayer when different player press the buton", async () => {
        let instance = await BeamButton.new(2);
        let latestPlayer = await instance.latestPlayer({from: accounts[0]});
        assert.equal(latestPlayer, 0);
        await instance.pressButton({from: accounts[0], value: 2}); 
        latestPlayer = await instance.latestPlayer({from: accounts[0]});
        assert.equal(latestPlayer, accounts[0]);
        await instance.pressButton({from: accounts[1], value: 2});
        latestPlayer = await instance.latestPlayer({from: accounts[0]});
        assert.equal(latestPlayer, accounts[1]);
    });
    
    it("pressButton from different accounts increases the treasure", async () => {
        let instance = await BeamButton.new(2);
        let treasure = await instance.treasure({from: accounts[0]});
        assert.equal(treasure, 0);
        await instance.pressButton({from: accounts[0], value: 2}) ;
        treasure = await instance.treasure({from: accounts[0]});
        assert.equal(treasure, 2);
        await instance.pressButton({from: accounts[1], value: 2}) 
        treasure = await instance.treasure({from: accounts[0]});
        assert.equal(treasure, 4);
    });
    
    it("pressButton won't work after the end", async () => {
        let instance = await BeamButton.new(2);
        await instance.pressButton({from: accounts[0], value: 2});
        await mineBlocks(3)
        await instance.claimTreasure({from: accounts[0]}) ;
        await truffleAssert.fails(
            instance.pressButton({from: accounts[0], value: 2}),
            truffleAssert.ErrorType.REVERT, 
            "Game already Ended"
            );
        });
        
    it("claimTreasure won't work before the start", async () => {
        let instance = await BeamButton.new(2);
        await truffleAssert.fails(
            instance.claimTreasure({from: accounts[0]}),
            truffleAssert.ErrorType.REVERT, 
            "Game still on"
            );
        });
        
    it("claimTreasure won't work after end", async () => {
        let instance = await BeamButton.new(2);
        await simulateGameOver(instance);
        await truffleAssert.fails(
            instance.claimTreasure({from: accounts[0]}),
            truffleAssert.ErrorType.REVERT, 
            "Game over. Treasure has been claimed"
            );
        });
    
    it("should reward winner, not the sender of claimTreasure", async () => {
        let instance = await BeamButton.new(2);
        await instance.pressButton({from: accounts[0], value: 2});
        await mineBlocks(3);
        result = await instance.claimTreasure({from: accounts[1]}),
        truffleAssert.eventEmitted(result, 'GameEnded', (ev) => {
            return ev.winner === accounts[0] && ev.amount.toNumber() === 2;
            });
        });
                    
    it("game will finish after 3 blocks even if claimTreasure wasn't", async () => {
        let instance = await BeamButton.new(2);
        await instance.pressButton({from: accounts[0], value: 2}) 
        await simulateGameOver(instance);
        await truffleAssert.fails(
            instance.pressButton({from: accounts[0], value: 2}),
            truffleAssert.ErrorType.REVERT, 
            "Game already Ended"
            );
    });
});           