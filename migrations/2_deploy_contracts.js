const BeamButton = artifacts.require("BeamButton.sol")
const argv = require('minimist')(process.argv.slice(2), {string: ['fixed_bid']});

module.exports = function(deployer) {
  deployer.deploy(BeamButton, argv['fixed_bid']);
};
